import io.Console;
import io.ConsoleMain;
import service.*;
import controller.Controller;


public class Main {
    public static void main(String[] args) {
        ConsoleMain console = new ConsoleMain();
        Service service = new Service();
        Controller controller = new Controller(console, service);
        controller.showMenu();
        controller.getMenuInputs();
    }
}

