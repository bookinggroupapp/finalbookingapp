package service;



import dao.*;
import entity.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Service {
    Scanner scan = new Scanner(System.in);
    int input;
    int idInput;
    String destination;
    String flightDate;
    int passengerCount;
    DAO<FlightSchedule> dao = new DAOFlightsFileText("flights.txt");
    //DAOBookingsText daoBooking;

    DAO<Bookings> dao1 = new DAOBookingsText("bookings.txt");
    DAO<Passengers> dao2 = new DAOPassengersText("passengers.txt");

    public int userId() {
        if (dao2.getAll().size()==0){
            return 1;
        }else{
            return dao2.getAll().size();
        }

    }

    public int bookingId() {
        if (dao1.getAll().size()==0){
            return 1;
        }else{
            return dao1.getAll().size();
        }

    }

    public void menu() {
        System.out.println("1. Online-Board");
        System.out.println("2. Show the Flight information");
        System.out.println("3. Search and book a flight");
        System.out.println("4. Cancel the booking");
        System.out.println("5. My Flights");
        System.out.println("6. Exit");
        System.out.println("0. Back to main Menu");
    }

    public void generateFlights() {
        dao.deleteFile();
        int flightID = 1;
        Random rand = new Random();
        int random;
        LocalDateTime fDtime;
        for (Cities c : Cities.values()) {
            random = rand.nextInt(24);
            fDtime = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES).plus(Duration.ofHours(random).minus(Duration.ofMinutes(random)));
            dao.create(new FlightSchedule(flightID++, c.name(), fDtime, random));
        }
    }

    public void printFlights() {
        if (dao.checkFlights(t -> t.flightTime.isAfter(LocalDateTime.now()))) generateFlights();
        System.out.println("----");
        dao.getAll().stream().forEach(System.out::println);
        System.out.println("----");
    }

    public void printFlightByID() {

            System.out.println("Please enter Flight ID: ");
            idInput = scan.nextInt();
            System.out.println(dao.get(idInput).toString());


    }

    public void flightBooking() {
        while (true) {

            try {
                System.out.println("Please enter destination address: "); destination=scan.next();
                System.out.println("Please enter flight date like this 'yyyy-mm-dd': "); flightDate=scan.next();
                System.out.println("Please enter passenger count: "); passengerCount=scan.nextInt();
                String str = dao.getAllBy(p ->
                        (p.Destination.equals(destination.toUpperCase()) && p.Seats >=
                                passengerCount && p.flightTime.toLocalDate().toString().equals(flightDate))).toString();
                if (str.length() < 3) {
                    System.out.println("There is no flights for your search criteria:");
                    System.out.println("Try again!! \n");
                    scan.nextLine();
                } else {
                    System.out.println(str);
                    for(int i=0; i<passengerCount; i++){
                        int bookingId = bookingId();
                        bookingId++;
                        int userId=userId();
                        userId++;
                        System.out.print("Please enter flight ID for booking: ");
                        int flightID = scan.nextInt();
                        System.out.println("Please enter name");
                        String name = scan.next();
                        System.out.println("Please enter surname");
                        String surname = scan.next();
                        dao2.create(new Passengers(userId, name, surname));
                        dao1.create(new Bookings(bookingId, flightID, userId));
                    }
                    System.out.println("Your booking has processed!! \n Returning to main menu");
                    menu();
                    break;
                }

            } catch (Exception ex) {
                System.out.println("Something went wrong");
                scan.nextLine();
            }
        }


    }

    public void flightCancel () {
        try {
            System.out.println("Please enter booking ID for canceling your flight: ");
            int bookingId = scan.nextInt();
            dao1.delete(bookingId);
            System.out.println("Your flight is cancelled");
        }catch (Exception ex){
            System.out.println("Not such booking id. Try again!");
            flightCancel();
        }
    }

    public void myFlightShow () {
        try {
            System.out.println("Enter your first name");
            String first = scan.next();
            System.out.println("Enter your last name");
            String last = scan.next();
            String[] str1;
            String str = dao2.getAllBy(p -> p.first.equals(first) && p.last.equals(last)).toString().replace("[", "").replace("]", "");
            System.out.println(str);
            str1 = str.split(";");
            int pid = Integer.parseInt(str1[0]);
            System.out.println(dao1.get(pid).toString());
        }catch(Exception ex){
            System.out.println("No passenger found. Try again!");
            myFlightShow();
        }
    }


    public void selectMenu() {
        do {
            input = scan.nextInt();
            switch (input) {
                case 1:
                    printFlights();
                    break;
                case 2:
                    printFlightByID();
                    break;
                case 3:
                    flightBooking();
                    break;
                case 4:
                    flightCancel();
                    break;
                case 5:
                    myFlightShow();
                    break;
                case 6:
                    System.out.println("Bye Bye");
                    break;
                case 0:
                    menu();
                    break;
            }
        } while (input != 6);
    }

}
