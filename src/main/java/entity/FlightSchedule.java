package entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FlightSchedule implements Serializable {
  public final int id;
  public final String Destination;
  public final LocalDateTime flightTime;
  public final int Seats;


  public FlightSchedule(int id, String Destination, LocalDateTime flightTime, int Seats) {
    this.id = id;
    this.Destination = Destination;
    this.flightTime = flightTime;
    this.Seats = Seats;
  }


  private static final long serialVersionUID = 1L;



  public static FlightSchedule parse(String line) {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    String[] chunks = line.split(";");
    return new FlightSchedule(
        Integer.parseInt(chunks[0]),
        chunks[1],
        LocalDateTime.parse(chunks[2], format),
        Integer.parseInt(chunks[3])
    );
  }

  public String represent() {
    return String.format("%d;%s;%s;%d", id, Destination, flightTime, Seats);
  }



  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (Destination != null ? Destination.hashCode() : 0);
    result = 31 * result + Seats;
    return result;
  }

  @Override
  public String toString() {
    return String.format("FlightNumber = %d | City = '%s' | FlightTime = %s | AvailableSeats = %d ", id, Destination, flightTime, Seats);
  }
}
