package entity;

import java.io.Serializable;

public class Bookings implements Serializable{

    public int id;
    public int flightId;
    public int passengerId;


    private static final long serialVersionUID = 1L;

    public Bookings(int id, int flightId,int passengerId) {
        this.id=id;
        this.flightId=flightId;
        this.passengerId=passengerId;
    }


    public static Bookings parse(String line) {
        String[] chunks = line.split(":");
        return new Bookings(
                Integer.parseInt(chunks[0]),
                Integer.parseInt(chunks[1]),
                Integer.parseInt(chunks[2])
        );
    }

    public String represent() {
        return String.format("%d:%d:%d",id, flightId, passengerId);

    }


    @Override
    public String toString() {
        return
               "BookingId= " + id +  ": Flight_id=" + flightId + ":" +  "PassengerId=" +  passengerId;
    }

}
