package entity;

import java.io.Serializable;

public class Passengers implements Serializable {
    public int passengerId;
    public String first;
    public String last;

    private static final long serialVersionUID = 1L;

    public Passengers(int passengerId, String first, String last) {
            this.passengerId = passengerId;
            this.first = first;
            this.last = last;
    }

    public static Passengers parse(String line) {
        String[] chunks = line.split(";");
        return new Passengers(
                Integer.parseInt(chunks[0]),
                chunks[1],
                chunks[2]
        );
    }

    public String represent() {
        return String.format("%d;%s;%s", passengerId, first, last);
    }


    @Override
    public int hashCode() {
        int result = passengerId;
        result = 31 * result + (first != null ? first.hashCode() : 0);
        result = 31 * result + first.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%d;%s;%s", passengerId, first, last);
    }

}
