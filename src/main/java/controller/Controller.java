package controller;


import service.*;
import io.Console;
import io.ConsoleMain;

public class Controller {
    Service service;
    Console console;

    public Controller(ConsoleMain console, Service service) {
        this.service = service;
        this.console = console;
    }


    public void show() {
        service.printFlights();
    }

    public void showMenu() {
        service.menu();
    }

    public void generateFlightList() {
        service.generateFlights();
    }

    public void getFlightById () {
        service.printFlightByID();
    }

    public void bookFlight() {
        service.flightBooking();
    }

    public void cancelFligth() {
        service.flightCancel();
    }

    public void getFlightByUserID() {
        service.myFlightShow();
    }

    public void search() {
        console.readLn();
    }

    public void book()
    {
        console.printLn("");
    }
    public void getMenuInputs() {
        service.selectMenu();
    }
}
