package dao;

import entity.Bookings;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DAOBookingsText implements DAO<Bookings> {

    private File file;
    //File file = new File("src/main/java/app/app/book.txt");

    public DAOBookingsText(String filename) {
        this.file = new File(filename);
    }

    @Override
    public Collection<Bookings> getAll() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            return br.lines().map(Bookings::parse).collect(Collectors.toList());
        } catch (IOException ex) {
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<Bookings> get(int passengerId) {
        return getAll().stream().filter(s -> s.passengerId == passengerId).findFirst();
    }

    @Override
    public Collection<Bookings> getAllBy(Predicate<Bookings> p) {
        return getAll().stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public boolean checkFlights(Predicate<Bookings> t) {
        if (getAll().stream().filter(t).collect(Collectors.toList()).isEmpty())
            return true;
        return false;
    }

    public void create(Bookings data){
        List<Bookings> bookings = new ArrayList<>();
        bookings.add(data);
        write(bookings);
    }

    private void write(Collection<Bookings> books) {
        try (FileWriter oos = new FileWriter(file,true)) {
            oos.append(books.stream().map(Bookings::represent).collect(Collectors.joining("\n")));
            oos.append("\n");
        } catch (IOException ex) {
            throw new RuntimeException("DAO:write:IOException", ex);
        }

    }




    @Override
    public void delete(int id) {
     Collection<Bookings> bookings = getAllBy(s -> s.id != id);
        file.delete();
        write(bookings);}

    @Override
    public void deleteFile() {
        file.delete();
    }
}



