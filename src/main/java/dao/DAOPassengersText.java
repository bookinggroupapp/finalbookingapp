package dao;

import entity.Passengers;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DAOPassengersText implements DAO<Passengers> {

    private File file;
    //File file = new File("src/main/java/app/app/book.txt");

    public DAOPassengersText(String filename) {
        this.file = new File(filename);
    }

    @Override
    public Collection<Passengers> getAll() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            return br.lines().map(Passengers::parse).collect(Collectors.toList());
        } catch (IOException ex) {
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<Passengers> get(int id) {
        return null; //getAll().stream().filter(s -> s.id == id).findFirst();
    }

    @Override
    public Collection<Passengers> getAllBy(Predicate<Passengers> p) {
        return getAll().stream().filter(p).collect(Collectors.toList());
    }


    public void create(Passengers data){
        List<Passengers> passengers = new ArrayList<>();
        passengers.add(data);
        write(passengers);
    }

    private void write(Collection<Passengers> books) {
        try (FileWriter oos = new FileWriter(file,true)) {
            oos.append(books.stream().map(Passengers::represent).collect(Collectors.joining("\n")));
            oos.append("\n");
        } catch (IOException ex) {
            throw new RuntimeException("DAO:write:IOException", ex);
        }

    }




    @Override
    public void delete(int id) {
     /*   Collection<Passengers> passengers = getAllBy(s -> s.id != id);
        write(passengers);}
    }*/
    }

    @Override
    public void deleteFile() {
        file.delete();
    }

    @Override
    public boolean checkFlights(Predicate<Passengers> t) {
        if (getAll().stream().filter(t).collect(Collectors.toList()).isEmpty())
            return true;
        return false;
    }
}
